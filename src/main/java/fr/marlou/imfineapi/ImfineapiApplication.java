package fr.marlou.imfineapi;

import fr.marlou.imfineapi.filter.ProfileFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class ImfineapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImfineapiApplication.class, args);
	}

	@Bean
	public FilterRegistrationBean<ProfileFilter> filterFilterProfileBean() {
		FilterRegistrationBean<ProfileFilter> registrationBean = new FilterRegistrationBean<>();
		registrationBean.setFilter(new ProfileFilter());
		registrationBean.addUrlPatterns("/profile");
		return registrationBean;
	}
}
