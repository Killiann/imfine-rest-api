package fr.marlou.imfineapi.repository;

import fr.marlou.imfineapi.model.Profile;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProfileRepository extends CrudRepository<Profile, Integer>, CustomProfileRepository {

    @Query("SELECT * FROM profile WHERE email = LOWER(:email)")
    Profile findByEmail(String email);
}
