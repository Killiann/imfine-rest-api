package fr.marlou.imfineapi.repository.impl;

import fr.marlou.imfineapi.exception.CustomException;
import fr.marlou.imfineapi.repository.CustomProfileRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;

@Repository
public class ProfileRepositoryImpl implements CustomProfileRepository {

    private final JdbcTemplate jdbcTemplate;

    public ProfileRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int create(String email, String password) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int i = jdbcTemplate.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO profile(email, password) VALUES (LOWER(?), ?)",
                    Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            return preparedStatement;
        }, keyHolder);

        if(i != 1)
            throw new CustomException("REGISTER_PROFILE");

        return Integer.parseInt(keyHolder.getKeys().get("GENERATED_KEY").toString());
    }
}
