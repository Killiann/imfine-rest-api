package fr.marlou.imfineapi.service;

import fr.marlou.imfineapi.model.AuthResult;
import fr.marlou.imfineapi.model.Profile;

import java.util.concurrent.CompletableFuture;

public interface ProfileService {

    CompletableFuture<Profile> login(String email, String password);

    CompletableFuture<AuthResult> register(Profile profile);

    CompletableFuture<Profile> find(int id);

    CompletableFuture<Boolean> update(int requestId, Profile profile);
}
