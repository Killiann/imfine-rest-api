package fr.marlou.imfineapi.service.impl;

import fr.marlou.imfineapi.exception.CustomException;
import fr.marlou.imfineapi.model.AuthResult;
import fr.marlou.imfineapi.repository.ProfileRepository;
import fr.marlou.imfineapi.service.ProfileService;
import fr.marlou.imfineapi.model.Profile;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.regex.Pattern;

@Service
public class ProfileServiceImpl implements ProfileService {

    private final ProfileRepository profileRepository;

    public ProfileServiceImpl(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    @Async
    @Override
    public CompletableFuture<Profile> login(String email, String password) {
        if(isEmailValid(email))
            throw new CustomException("INVALID_EMAIL");

        Profile profile = profileRepository.findByEmail(email);
        if(profile == null || !BCrypt.checkpw(password, profile.getPassword()))
            throw new CustomException("INVALID_PASSWORD_EMAIL");

        return CompletableFuture.completedFuture(profile);
    }

    @Async
    @Override
    public CompletableFuture<AuthResult> register(Profile profile) {
        if(isEmailValid(profile.getEmail()))
            throw new CustomException("INVALID_EMAIL");

        if(profile.getPassword().length() != 4)
            throw new CustomException("INVALID_PASSWORD");

        profile.setPassword(BCrypt.hashpw(profile.getPassword(), BCrypt.gensalt(10)));
        try {
            profile.setId(profileRepository.create(profile.getEmail(), profile.getPassword()));
        } catch (DataIntegrityViolationException e) {
            throw new CustomException("ACOUNT_EXIST");
        }

        if(profile.getId() < 1)
            throw new CustomException("INVALID_PROFILE");

        return CompletableFuture.completedFuture(new AuthResult(profile.generateToken()));
    }

    @Async
    @Override
    public CompletableFuture<Profile> find(int id) {
        if(id < 1)
            throw new CustomException("INVALID_ID");
        return CompletableFuture.completedFuture(profileRepository.findById(id)
                .orElseThrow(() -> new CustomException("INVALID_PROFILE")));
    }

    @Async
    @Override
    public CompletableFuture<Boolean> update(int requestId, Profile profile) {
        Profile storedProfile = this.find(requestId).join();

        if(profile.getPassword() != null && profile.getPassword().length() == 4 && !BCrypt.checkpw(profile.getPassword(), storedProfile.getPassword())) {
            storedProfile.setPassword(BCrypt.hashpw(profile.getPassword(), BCrypt.gensalt(10)));
        }

        if(different(storedProfile.getMale(), profile.getMale())) {
            storedProfile.setMale(profile.getMale());
        }

        if(different(storedProfile.getName(), profile.getName())) {
            storedProfile.setName(profile.getName());
        }

        if(different(storedProfile.getWeight(), profile.getWeight())) {
            storedProfile.setWeight(profile.getWeight());
        }
        profileRepository.save(storedProfile);

        return CompletableFuture.completedFuture(true);
    }

    private boolean isEmailValid(String email) {
        Pattern pattern = Pattern.compile("^(.+)@(.+)$");
        return !pattern.matcher(email).matches();
    }

    private <T> boolean different(T value, T valueModified) {
        return valueModified != null && (value == null || !value.equals(valueModified));
    }
}
