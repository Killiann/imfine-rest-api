package fr.marlou.imfineapi;

import fr.marlou.imfineapi.model.AuthResult;
import fr.marlou.imfineapi.model.Profile;
import fr.marlou.imfineapi.service.ProfileService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@RestController
public class ProfileController {

    private final ProfileService profileService;

    public ProfileController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @PostMapping("/login")
    CompletableFuture<ResponseEntity<AuthResult>> login(@RequestBody Map<String, String> body) {
        return profileService.login(body.get("email"), body.get("password"))
                .thenApply(profile -> new ResponseEntity<>(new AuthResult(profile.generateToken()), HttpStatus.OK));
    }

    @PostMapping("/register")
    CompletableFuture<ResponseEntity<AuthResult>> register(@RequestBody Profile requestProfile){
        return profileService.register(requestProfile)
                .thenApply(result -> new ResponseEntity<>(result, HttpStatus.OK));
    }

    @GetMapping("/profile")
    CompletableFuture<ResponseEntity<Profile>> find(HttpServletRequest request){
        return profileService.find((int) request.getAttribute("id"))
                .thenApply(profile -> new ResponseEntity<>(profile, HttpStatus.OK));
    }

    @PostMapping("/profile")
    CompletableFuture<ResponseEntity<Boolean>> update(HttpServletRequest request, @RequestBody Profile profile) {
        return profileService.update((int) request.getAttribute("id"), profile)
                .thenApply(aBoolean -> new ResponseEntity<>(aBoolean, HttpStatus.OK));
    }
}
