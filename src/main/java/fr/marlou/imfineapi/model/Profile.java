package fr.marlou.imfineapi.model;

import fr.marlou.imfineapi.Constants;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table
public class Profile implements Serializable {

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue
    private Integer id;

    @Column(unique = true)
    private String email;

    @Column
    private String password;

    @Column
    private String name;

    @Column
    private Boolean male;

    @Column
    private Integer weight;

    public String generateToken() {
        return Jwts.builder().signWith(SignatureAlgorithm.RS256, Constants.PRIVATE_KEY)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + Constants.TOKEN_VALIDITY))
                .claim("id", this.id)
                .compact();
    }
}
