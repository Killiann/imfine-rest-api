package fr.marlou.imfineapi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class AuthResult {
    private final String key;
}
