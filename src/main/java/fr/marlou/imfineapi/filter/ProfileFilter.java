package fr.marlou.imfineapi.filter;

import fr.marlou.imfineapi.Constants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AllArgsConstructor
public class ProfileFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        String token = httpServletRequest.getHeader("Authorization");
        if(token != null) {
            try {
                token = token.split("Bearer ")[1];
                Claims claims = Jwts.parser().setSigningKey(Constants.PRIVATE_KEY).parseClaimsJws(token).getBody();
                httpServletRequest.setAttribute("id", Integer.parseInt(claims.get("id").toString()));
                httpServletRequest.setAttribute("token", token);
            } catch (Exception e) {
                httpServletResponse.sendError(HttpStatus.FORBIDDEN.value(), "INVALID_TOKEN");
                return;
            }
        } else {
            httpServletResponse.sendError(HttpStatus.FORBIDDEN.value(), "INVALID_TOKEN");
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }
}
