package fr.marlou.imfineapi;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

public class Constants {

    public final static long TOKEN_VALIDITY = 600000000L;

    public static PrivateKey PRIVATE_KEY;

    static {
        try {
            String rsaPrivateKey = System.getenv("rsaPrivateKey");

            rsaPrivateKey = rsaPrivateKey.replace("-----BEGIN PRIVATE KEY-----", "");
            rsaPrivateKey = rsaPrivateKey.replace("-----END PRIVATE KEY-----", "");

            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(rsaPrivateKey));
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PRIVATE_KEY = kf.generatePrivate(keySpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
